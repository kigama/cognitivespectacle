import React, { Component } from 'react';

 
function Video(props){
    return(
        <div className='embed-responsive embed-responsive-4by3'>
            <iframe className='embed-responsive-item' src={props.url} ></iframe>
          </div>
    )
}



export default Video;