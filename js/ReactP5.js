import React from "react";
import Sketch from "react-p5";

const P5Manager =(props)=> {
  const setup = (p5, canvasParentRef) => {
    p5.createCanvas(600, 400).parent(canvasParentRef);
	};
	const draw = (p5) => {
		p5.background(255);         
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default P5Manager;