# team-visual
  - Jose Perdomo Saenz @jdperdomos (github)
  - Kevin Alejandro Vanegas @kavanegasm (gitlab) @kigama (github)

# Contexto

Como apoyo a la asignatura Computación Visual dada en la Universidad Nacional de Colombia, fueron realizadas una serie de presentaciones que son de suma importancia para la estructura y ejecución del curso, las cuales están desarrolladas en Reveal JS 3.x y son accesibles mediante el siguiente enlace en GitHub https://github.com/VisualComputing/Cognitive, pero teniendo en cuenta los continuos y rápidos cambios que suceden en el entorno tecnológico, es siempre necesario tener cierta flexibilidad para la implementación de nuevas actualizaciones en los lenguajes de programación, librerías y/o frameworks, para con ello evitar que las novedades y tendencias tecnológicas terminen por dejar a estos proyectos obsoletos, en ese sentido surge un interés por la implementación de actualizaciones a la base del código requerido para el despliegue y desarrollo de de las presentaciones del curso.

# Trabajo Previo

Durante el transcurso del curso se han podido adquirir importantes bases teóricas y prácticas referentes al estudio y la aplicación de la computación visual, conocimientos que de ser aplicados de forma correcta, nos permiten tener la posibilidad de generar valor en la industria de la tecnología, puntualmente nos hemos familiarizado con lenguajes y librerías como lo son processing, p5, d3 que son importantes en el desarrollo del componente visual de una aplicación y junto a otros conocimientos adquiridos previamente dentro de la academia así como fuera de ella, en lenguajes relacionados como Javascript, Java y React, nos brinda un repertorio interesante de herramientas para abordar este proyecto.

# Problemática

Actualmente se presentan Inconvenientes para la actualización de las presentaciones de la materia, en el caso específico del proyecto Cognitive los Sketches dejaban de funcionar en las últimas versiones de Reveal JS, por lo cual corre el riesgo que en algún momento, las funcionalidades allí desarrolladas entren en un estado de desactualización.

# Objetivo

Realizar una actualización de la base de las presentaciones, sin alterar significativamente su funcionalidad y aspecto.

# Diseño de la solución

Para realizar el ejercicio objetivo de este proyecto, se propone actualizar las presentaciones, para ser utilizadas con Spectacle el cual es una librería basada en React.js para presentaciones.


# Demo
https://kigama.gitlab.io/cognitivespectacle

# Conclusiones

El demo realizado con la implementación de las presentaciones en Spectacle, pone a disposición una base que puede ser de gran utilidad, para la ejecución de un plan completo de actualización del repositorio.

Aunque implementar Spectacle por primera vez genera una curva de aprendizaje un poco alta, más adelante puede ser altamente escalable para desarrollar presentaciones más complejas debido que al CERN implementa esta libreria en sus desarrollos [https://slides.docs.cern.ch/]

# Resultados

La migración directa de las versiones Reveal js 3.x a 4.x deja inservibles los Sketches, tampoco fue posible montar los Sketches de forma satisfactoria, al realizar la adaptación directa del contenido desde la base 4.x. Se realizó la adaptación del material en Spectacle, en donde se puede observar que, desde allí, todos los Sketches funcionan correctamente si estos han sido realizados en react-p5, se creó un módulo capaz de mostrar los videos de la presentación dado que nativamente Spectacle no lo soporta, por otra parte como inconveniente generado en la actualización, se perdió el desplazamiento vertical de las diapositivas.

# Trabajo Futuro

Se busca darle mayores funcionalidades a las presentaciones del curso que permitan apoyar y extender la experiencia de los estudiantes y personas interesadas en el tema. Modificar los estilos de los elementos de las diapositivas para que sean más amenas.


