/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/26605*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */
// Ponzo ilusion
// Author: Mario Ponzo
// August 23, 2017
// Ponzo, M.(1911)
// "Intorno ad alcune illusioni nel campo delle sensazioni tattili sull'illusione di Aristotele e fenomeni analoghi". Archives Italiennes de Biologie.
// Sketch made by Neill Giraldo : http://neillgiraldo.github.io/
import React from "react";
import Sketch from "react-p5";

const Ponzo =(props)=> {
  	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(550, 400).parent(canvasParentRef);
    p5.smooth();               // antialias lines
    p5.noLoop();
        // red strips
        p5.mousePressed = function () {
            p5.fill("#FF0000");
            p5.strokeWeight(0);
        
            p5.rect(198, 0, 3, 400);
            p5.rect(343, 0, 3, 400);
    }  
    p5.mouseReleased = function () {
        p5.draw();
    }
    };

	const draw = (p5) => {
		//style
        p5.background(180);
        p5.strokeWeight(0);
        
        p5.fill(0);      // medium weight lines 
 
        // Vertical/diagonal black thick lines
        // downleft,upleft,upright, downright
        p5.quad(85,400,255,0,260,0,115,400);
        p5.quad(440,400,280,0,285,0,470,400);

        //style
        p5.stroke(0);
        p5.strokeWeight(0);
        p5.fill(0);

        // Horizontal Black lines

        let startx = 250;
        let starty = 0.2;
        let startwx = 40;
        let startwy = 0.5;
        let index = 1;
        while(starty < 400){
            p5.rect(startx,starty,startwx,startwy,5);
            startx = startx - (starty * 1.35 -starty)*0.65;
            starty = starty * Math.pow(index,0.1);
            startwy = startwy + (starty * 1.6 -starty)*0.015;
            startwx = startwx + (starty * 1.4 -starty)*0.85;
            index = index + 1;
        }

        p5.fill(255, 255, 0);

        // yellow strips
        p5.rect(198,40,145,10);
        p5.rect(198,330,145,10);

        p5.fill(0);
        
        p5.line(200,550,250,0);
        p5.line(300,550,270,0);

        p5.stroke(0);
        p5.strokeWeight(5);       // medium weight lines 

        // four white lines
        p5.stroke(255);
        p5.strokeWeight(10);
        p5.line(0,0,0,400);
        p5.line(0,0,550,0);
        p5.line(0,400,550,400);
        p5.line(550,0,550,400);

        p5.filter("blur",1);

    };

  return(<Sketch setup={setup} draw={draw}/>)

}
export default Ponzo;