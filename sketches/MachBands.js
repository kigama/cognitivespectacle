//Author: Edwin Alexander Bohorquez Gamba
import React from "react";
import Sketch from "react-p5";

const MachBands =(props)=> {
  let height_rect = 70;
	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(600, 400).parent(canvasParentRef);
    p5.smooth();
    p5.mouseMoved = function() {        
        let dy = p5.mouseY - p5.pmouseY;
        if( dy < 0 ){        
            height_rect -= 10;    
        }else{
            if( dy > 0 ){                
                height_rect += 10;      
            }
        }                 
    };
    };  

	const draw = (p5) => {
		p5.background(255);         
        if( height_rect < 35 )
            height_rect = 35;
        if( height_rect > 70 )
            height_rect = 70;
        p5.noStroke();        
        p5.fill(66,66,66);
        p5.rect(200,20,200,height_rect);
        p5.fill(86,86,86);
        p5.rect(160,90,280,height_rect);
        p5.fill(122,122,122);
        p5.rect(120,160,360,height_rect);
        p5.fill(152,152,152);
        p5.rect(80,230,440,height_rect);
        p5.fill(182,182,182);
        p5.rect(40,300,520,height_rect);           
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default MachBands;


