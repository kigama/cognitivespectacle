import React from "react";
import Sketch from "react-p5";

const Ebbinghaus =(props)=> {  
	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(800, 400).parent(canvasParentRef);
    p5.background(127);
	};
	const draw = (p5) => {
		p5.background(127);
      p5.noStroke();
      
      p5.fill(225,128,0);
      p5.ellipse( 250,200, 60, 60 );
      p5.ellipse( 650,200, 60, 60 );
      
      if( !p5.mouseIsPressed ) {
        p5.fill(153,204,255);
        p5.ellipse( 100,200, 135, 135 );
        p5.ellipse( 177,320, 135, 135 );
        p5.ellipse( 323,320, 135, 135 );
        p5.ellipse( 400,200, 135, 135 );
        p5.ellipse( 177,70, 135, 135 );
        p5.ellipse( 323,70, 135, 135 );
        
        p5.ellipse( 600,200, 25, 25 );
        p5.ellipse( 615,235, 25, 25 );
        p5.ellipse( 650,250, 25, 25 );
        p5.ellipse( 685,235, 25, 25 );
        p5.ellipse( 700,200, 25, 25 );
        p5.ellipse( 685,165, 25, 25 );
        p5.ellipse( 650,150, 25, 25 );
        p5.ellipse( 615,165, 25, 25 );
      }  
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default Ebbinghaus;

