import React from "react";
import Sketch from "react-p5";

const P5Manager =(props)=> {
  const setup = (p5, canvasParentRef) => {
    p5.createCanvas(600, 400).parent(canvasParentRef);
    p5.background(255);
	};
	const draw = (p5) => {
		p5.background(255);
      makeParallelLines(p5);
      if( !p5.mouseIsPressed ) {
        makeIlussionLines(p5);
      }
      //cleaner();         
  };
  function makeParallelLines(p5) {
    p5.strokeWeight(15);
    p5.strokeCap(p5.SQUARE); 
    let aux = 100;
    let x = [100-aux,200-aux];
    let y = [500, 600];
    let d = 100;
    
    for(let i = 0; i < 11; i++) {
      p5.line(x[0],y[0]-i*d, x[1]+i*d,y[1]);
    }
  }


  function makeIlussionLines(p5) {
    makeHorizontalLines(p5);
    makeVerticalLines(p5);
  }

  function makeHorizontalLines(p5) {
    let qx = [70, 130, 115, 55];
    let qy = [600,600,585,585];
    p5.strokeWeight(1);
    p5.fill(0);
    for(let i = 0; i < 6; i++) {
      for(let j = 0; 0 <= qx[0]+(200*i)-(j*30); j++) {
        p5.quad(qx[0]+(200*i)-(j*30), qy[0]-(j*30), qx[1]+(200*i)-(j*30), qy[1]-(j*30), qx[2]+(200*i)-(j*30), qy[2]-(j*30), qx[3]+(200*i)-(j*30), qy[3]-(j*30));
      }
    }
    
  }

  function makeVerticalLines(p5) {
    let qx = [0, 0, 15, 15];
    let qy = [430, 370, 385, 445];
    p5.strokeWeight(1);
    p5.fill(0);
    for(let i = 0; i < 6; i++) {
      for(let j = 0; 610 >= qy[3]+(j*30)-(200*i); j++) {
        p5.quad(qx[0]+(j*30), qy[0]+(j*30)-(200*i), qx[1]+(j*30), qy[1]+(j*30)-(200*i), qx[2]+(j*30), qy[2]+(j*30)-(200*i), qx[3]+(j*30), qy[3]+(j*30)-(200*i));
      }
    }
  }

  function cleaner(p5) {
    p5.fill(255);
    p5.noStroke();
    p5.rect(0,0,650,94);
    p5.rect(706,100,200,600);
  };
  return(<Sketch setup={setup} draw={draw}/>)

}
export default P5Manager;

