// Lilac Chaser
// Author: Jeremy Hinton
// Before 2005
// A sent message to Michael Bach
// https://en.wikipedia.org/wiki/Lilac_chaser
// JS port (p5.js 'instance mode') by Sergio Andres Castro
import React from "react";
import Sketch from "react-p5";
const LilacChaser =(props)=> {
  let jump = 0;
  let count = 0;
	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(600, 400).parent(canvasParentRef);
    p5.frameRate(7);
	};
  const draw = (p5) => {
    p5.background(200);
    let numCircles = 12;
    let stepAngle = 360.0/numCircles;
    p5.push();
    p5.translate(p5.width/2.0, p5.height/2.0);
    for (let i = 0; i < 360; i = i + stepAngle) {
      //console.log(stepAngle, i, jump);
      if (i != jump) {
        p5.push();
        p5.rotate(p5.radians(i));
        drawBlurCircles(p5,120, 0, 60);
        p5.pop();
      }
    }
    if( !p5.mouseIsPressed ) {
      jump = (jump + stepAngle)%360;
    }
    p5.push();
    p5.strokeWeight(1.5);
    p5.line(-7, 0, 7, 0);
    p5.line(0, -7, 0, 7);
    p5.pop();
    p5.pop();
  };
  function drawBlurCircles(p5,x, y, r) {
    p5.push();
    p5.push();
    //console.log(1);
    p5.noStroke();
    let opc = 0.4;
    let step = 3.0/r;

    for (let i = r; i > 0; i-=1.5) {
      if (opc < 5) {
        opc += step;
        p5.fill(255, 20, 180, opc);
        //console.log(step,r);
      }
      //console.log(4);
      p5.ellipse(x, y, i, i);
    }
    p5.pop();
    p5.pop();
  };
return(<Sketch setup={setup} draw={draw}/>)
};
export default LilacChaser;
