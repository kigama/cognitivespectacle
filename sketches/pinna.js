// Pinna Illusion (Spiral Effect)
// Author: Dr. Biangio Pinna
// (Pinna & Gregory 2002)
// Posterior work over original illusion
// http://www.scholarpedia.org/article/Pinna_illusion
// JS port (p5.js 'instance mode') by Sergio Andres Castro
import React from "react";
import Sketch from "react-p5";

const Pinna =(props)=> {  
	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(400, 400).parent(canvasParentRef);
	};
	const draw = (p5) => {
		p5.background(127);
    p5.push();
    p5.translate(p5.width/2.0, p5.height/2.0);
    circleOfRects(p5,38, 11, 12, 16);
    circleOfRects(p5,75, 11, -20, 28);
    circleOfRects(p5,102, 11, 10, 42);
    circleOfRects(p5,140, 11, -20, 56);
    if ( p5.mouseIsPressed ){
      p5.push();
      p5.strokeWeight(3);
      p5.stroke(255);
      p5.noFill();
      p5.ellipse(0,0,125,125);
      p5.ellipse(0,0,185,185);
      p5.ellipse(0,0,255,255);
    };
    p5.pop();
  };        
  
  function circleOfRects(p5,y, r, rot, numRect) {
    let count = 0;
    let step = 360/numRect;
    for (let i = 0; i < 360; i += step) {
      p5.push();
      p5.push();
      p5.noFill();
      if (count % 2 == 0) {
        p5.stroke(255);
      } else {
        p5.stroke(0);
      };
      p5.rotate(p5.radians(i));
      p5.push();
      p5.translate(0, y);
      p5.rotate(p5.radians(rot));
      p5.rect(0, 0, r, r);
      p5.pop();
      p5.pop();
      p5.pop();
      count+=1;
    };
  };

  return(<Sketch setup={setup} draw={draw}/>)
}
export default Pinna;