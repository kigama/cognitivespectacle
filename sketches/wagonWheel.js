// Wagon Wheel (stagecoach-wheel effect or stroboscopic effect)
// Author: William Albert Hugh Rushton
// December 23 of 1967
// Effect of Humming on Vision
// https://www.nature.com/nature/journal/v216/n5121/abs/2161173a0.html
// https://en.wikipedia.org/wiki/Wagon-wheel_effect
// JS port (p5.js 'instance mode') by Sergio Andres Castro
import React from "react";
import Sketch from "react-p5";

const WagonWheel =(props)=> {
  let WWAccel = 0.2;
  let WWSpeed = 0.0;
  const setup = (p5, canvasParentRef) => {
    p5.createCanvas(400, 400).parent(canvasParentRef);
	};
	const draw = (p5) => {
		p5.background(255);
    p5.push();
    p5.fill(0);
    p5.translate(p5.width/2.0, p5.height/2.0);
    for (let i = 0; i < 360; i = i + 72) {
      p5.push();
      p5.rotate(p5.radians(i+180+WWSpeed));
      p5.rect(-6.25, 12.5, 12.5, 45);
      p5.pop();
    };
    p5.fill(0);
    for (let i = 0; i < 360; i = i + 60) {
      p5.push();
      p5.rotate(p5.radians(i+180+WWSpeed));
      p5.rect(-6.25, 57.5, 12.5, 90);
      p5.pop();
    };
    p5.pop();
    if ( !p5.mouseIsPressed ) {
      WWSpeed += WWAccel;
      WWAccel +=0.2;
    } else {
      WWAccel = 0;
    };       
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default WagonWheel;


