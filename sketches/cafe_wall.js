import React from "react";
import Sketch from "react-p5";

const cafe_wall =(props)=> {
// Author: Jairo Suarez
// February 16, 2017
    let cWidth = 30; // cell width
    let cHeight = 26; // cell height
    let linexHeight = 1; // stroke width for horizontal
    let lineyHeight = 2; // stroke width for vertical lines
    let rows = 9; // number of rows to draw
    let canvasWidth = 400; // canvas width
   
	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(canvasWidth, (cHeight + lineyHeight) * rows + lineyHeight).parent(canvasParentRef);
    p5.background('#888');
    p5.smooth();
    p5.noLoop();
    // draw the cell each time the mouse is moved
    p5.mouseMoved = function() {
        p5.draw();
    }
    };
        // draw a row of cells
        function drawRow(p5, row) {
            let yPos = row * (cHeight + lineyHeight) + lineyHeight;
            let numCells = Math.ceil(canvasWidth / cWidth) + 3;
            for (let i = -80; i < numCells; i = i + 1) {
                if (i % 2 == 0) {
                    p5.fill(0);
                } else {
                    p5.fill('#fff');
                }
                p5.noStroke();
                let pos = row % 4;
                if (pos == 3) pos = 1;
                p5.rect(
                    i * (cWidth + linexHeight) - pos * p5.mouseX / 15 % (2 * cWidth) + 15,
                    yPos,
                    cWidth,
                    cHeight
                );
            }
        };
    
	const draw = (p5) => {
        p5.background('#888');
        for (let i = 0; i < rows; i = i + 1) {
            drawRow(p5,i);
        }        

	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default cafe_wall;