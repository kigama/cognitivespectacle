// Author: Jairo Suarez
// February 16, 2017
import React from "react";
import Sketch from "react-p5";

const colorGradient =(props)=> {
  let c1,c2,c3,w,h;
	const setup = (p5, canvasParentRef) => {
        w = 440; // canvas width
        h = 320; // canvas height
        // Here we use [Web Colors](https://en.wikipedia.org/wiki/Web_colors) using the #RRGGBB model
        c1 = "#323232"; // left color
        c2 = "#C7C7C7"; // right color
        c3 = "#787878"; // block color
        p5.createCanvas(w, h).parent(canvasParentRef);
        p5.smooth();
        p5.noLoop();
        // this sets a listener to the mouse click function and toggles the gradient
        p5.mouseClicked = function() {
        if (c2 == "#C7C7C7") {
            c2 = "#323232";
        } else {
            c2 = "#C7C7C7";
        }
        p5.draw();
    };
        };
	const draw = (p5) => {
        setGradient(p5, c1, c2);
        p5.noStroke();
        p5.fill(c3);
        p5.rect(30, 140, 380, 40);       
    };
    // adapted from: https://p5js.org/examples/color-linear-gradient.html
    // this function draws a left to right gradient interpolating the values between two colors (c1,c2)
    function setGradient(p5,c1, c2) {
        p5.noFill();
        // Left to right gradient
        for (var i = 0; i <=  w; i++) {
            var inter = p5.map(i, 0, w, 0, 1);
            var c = p5.lerpColor(p5.color(c1), p5.color(c2), inter);
            p5.stroke(c);
            p5.line(i, 0, i, h);
        }
    };


  return(<Sketch setup={setup} draw={draw}/>)

}
export default colorGradient;    