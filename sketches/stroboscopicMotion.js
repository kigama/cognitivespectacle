/**
  * Author: Carlos Tovar Bonilla
  * September 20, 2017
  *
  * Stroboscopic Motion
  *
**/
import React from "react";
import Sketch from "react-p5";

const Stroboscopic =(props)=> {
let firstPair;
  const setup = (p5, canvasParentRef) => {
    p5.createCanvas(600, 600).parent(canvasParentRef);
	};
	const draw = (p5) => {
        p5.background(192, 255, 255);
        p5.frameRate(2);
        stroboscopicMotion(p5);       
    };
    //Funcion principal, pinta un par de puntos según el estado de @firstPair
    function stroboscopicMotion(p5) {
        p5.background(192, 192, 192);
        p5.strokeWeight(100);
        p5.stroke(0, 0, 255);
        if (firstPair) {
            p5.point(150, 150);
            p5.point(450, 450);
        } else {
            p5.point(450, 150);
            p5.point(150, 450);
        }
        firstPair = !firstPair;
    };
  return(<Sketch setup={setup} draw={draw}/>)

}
export default Stroboscopic;


