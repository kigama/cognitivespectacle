import React from "react";
import Sketch from "react-p5";

const MullerLyer =(props)=> {
let mld = 20;
let mlx = [100,280];
let mly = [100,200,300];
	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(400, 400).parent(canvasParentRef);
    p5.background(255);
	};
	const draw = (p5) => {
    p5.background(255);
    p5.stroke(0);
    p5.strokeWeight(4);
    //Horizontal lines
    for(let i = 0; i < mly.length; i++) {
      p5.line( mlx[0], mly[i], mlx[1], mly[i] ); 
    }
    //Top line
    p5.line( mlx[0], mly[0], mlx[0]+mld, mly[0]+mld );
    p5.line( mlx[0], mly[0], mlx[0]+mld, mly[0]-mld );
    p5.line( mlx[1], mly[0], mlx[1]-mld, mly[0]+mld );
    p5.line( mlx[1], mly[0], mlx[1]-mld, mly[0]-mld );
  
    //Medium line
    p5.line( mlx[0], mly[1], mlx[0]-mld, mly[1]+mld );
    p5.line( mlx[0], mly[1], mlx[0]-mld, mly[1]-mld );
    p5.line( mlx[1], mly[1], mlx[1]+mld, mly[1]+mld );
    p5.line( mlx[1], mly[1], mlx[1]+mld, mly[1]-mld );
      
    //Bottom line
    p5.line( mlx[0], mly[2], mlx[0]+mld, mly[2]+mld );
    p5.line( mlx[0], mly[2], mlx[0]+mld, mly[2]-mld );
    p5.line( mlx[1], mly[2], mlx[1]+mld, mly[2]+mld );
    p5.line( mlx[1], mly[2], mlx[1]+mld, mly[2]-mld );
  
    if (p5.mouseIsPressed == true) {
      p5.stroke(255,0,0);
      p5.strokeWeight(2);
      p5.line(mlx[0], mly[0], mlx[0], mly[2]);
      p5.line(mlx[1], mly[0], mlx[1], mly[2]);
    }         
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default MullerLyer;

