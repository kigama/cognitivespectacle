//Author: Edwin Alexander Bohorquez Gamba
import React from "react";
import Sketch from "react-p5";

const PenroseTriangle =(props)=> {
let color1 = '#FF0000';
let color2 = '#00FF00';
let color3 = '#0000FF';
let aux = '';

	const setup = (p5, canvasParentRef) => {
        p5.createCanvas(600, 390).parent(canvasParentRef);
        p5.smooth();
        p5.mouseClicked = function() {                
            aux = color1;
            color1 = color3;
            color3 = color2;                
            color2 = aux;                
        };
	};
	const draw = (p5) => {
		p5.background(255);          
        p5.strokeWeight(3);                

        p5.beginShape();        
        p5.fill(color1);        
        p5.vertex(270, 25);
        p5.vertex(108, 316);
        p5.vertex(137, 368);
        p5.vertex(271, 129);
        p5.vertex(346, 264);
        p5.vertex(405, 264);
        p5.vertex(270, 25);                
        p5.endShape();

        p5.beginShape();
        p5.fill(color2);
        p5.vertex(270, 25);
        p5.vertex(329,25);
        p5.vertex(491,316);
        p5.vertex(228,316);
        p5.vertex(257,264);
        p5.vertex(405,264);
        p5.vertex(270, 25);
        p5.endShape();
        
        p5.beginShape();
        p5.fill(color3);
        p5.vertex(300, 182);
        p5.vertex(228,316); 
        p5.vertex(491,316);
        p5.vertex(467, 368);
        p5.vertex(137, 368);             
        p5.vertex(271, 129);
        p5.vertex(300, 182);
        p5.endShape();        
          
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default PenroseTriangle;

