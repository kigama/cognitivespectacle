//Víctor Ramírez, Marzo 02, 2017
//Implementación desde cero
import React from "react";
import Sketch from "react-p5";

const Watercolor =(props)=> {
    let lines = [//for alien figure
        [0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 5, 2, 2, 3, 2, 4, 1, 3, 0, 3, 1, 3, 0, 2, 3, 3],//x1
        [4, 3, 5, 0, 2, 5, 0, 1, 3, 6, 7, 1, 3, 7, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 6, 7, 7, 7, 8],//y1
        [0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 5, 3, 3, 4, 3, 7, 2, 4, 1, 4, 2, 8, 1, 3, 5, 5],//x2
        [7, 4, 7, 1, 3, 7, 1, 2, 4, 7, 8, 2, 4, 8, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 6, 7, 7, 7, 8],//y2
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],//is horizontal
        [1, 1, -1, 1, 1, 1, -1, 1, -1, -1, 1, -1, 1, -1, 1, -1, 1, 1, 1, 1, -1, 1, 1, -1, -1, -1, -1, 1, -1],//color (1)down or right or (-1)up or left  
    ];
  const setup = (p5, canvasParentRef) => {
    p5.createCanvas(350, 350).parent(canvasParentRef);
	};
	const draw = (p5) => {
		p5.background( 255 );
        let pixelSz = 9;
        drawAlien(p5, 50, 60, pixelSz, 0x78, 0xff, 0x02, 0x89, 0x02, 0xff, !mouseOver(p5, 50, 60, pixelSz ));
        drawAlien(p5, 50, 220, pixelSz, 0xff, 0x65, 0xd9, 0x47, 0xb5, 0x62, !mouseOver(p5, 50, 220, pixelSz ) );
        drawAlien(p5, 200, 60, pixelSz, 0xd9, 0xff, 0x03, 0x29, 0x03, 0xff, !mouseOver(p5, 200, 60, pixelSz ) );
        drawAlien(p5, 200, 220, pixelSz, 0x5f, 0xff, 0xf7, 0xb3, 0x42, 0x48, !mouseOver(p5, 200, 220, pixelSz ) );        
    };
    function mouseOver(p5, x, y, pixelSz ){
        return p5.mouseX >= x && p5.mouseX <= x + 11*pixelSz && p5.mouseY >= y && p5.mouseY <= y + 8*pixelSz;
    };

    function drawAlien(p5, x, y, pixelSz, r1, g1, b1, r2, g2, b2, useColor ){
        let lineSz = 2;
        p5.strokeWeight( lineSz );
        if( useColor ){
            for( let i = 0; i < lines[0].length; i++ ){
                p5.stroke( r1, g1, b1 );
                if( lines[4][i] == 0 ){
                    p5.line( x + pixelSz*lines[0][i] + lineSz*lines[5][i], y + pixelSz*lines[1][i], x + pixelSz*lines[2][i] + lineSz*lines[5][i], y + pixelSz*lines[3][i]);
                    p5.line( x + pixelSz*(11 - lines[0][i]) - lineSz*lines[5][i], y + pixelSz*lines[1][i], x + pixelSz*(11 - lines[2][i]) - lineSz*lines[5][i], y + pixelSz*lines[3][i] );
                }
                else{
                    p5.line( x + pixelSz*lines[0][i], y + pixelSz*lines[1][i] + lineSz*lines[5][i], x + pixelSz*lines[2][i], y + pixelSz*lines[3][i] + lineSz*lines[5][i] );
                    p5.line( x + pixelSz*(11 - lines[0][i]), y + pixelSz*lines[1][i] + lineSz*lines[5][i], x + pixelSz*(11 - lines[2][i]), y + pixelSz*lines[3][i] + lineSz*lines[5][i] );
                }
            }
        }
        for( let i = 0; i < lines[0].length; i++ ){
            p5.stroke( r2, g2, b2 );
            p5.line( x + pixelSz*lines[0][i], y + pixelSz*lines[1][i], x + pixelSz*lines[2][i], y + pixelSz*lines[3][i] );
            p5.line( x + pixelSz*(11 - lines[0][i]), y + pixelSz*lines[1][i], x + pixelSz*(11 - lines[2][i]), y + pixelSz*lines[3][i] );
        }
    };
  return(<Sketch setup={setup} draw={draw}/>)

}
export default Watercolor;


 
