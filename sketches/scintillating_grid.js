/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/26605*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */
// Scintillating Grid 
// Author: Rupert Russell
// October 2, 2010
// Schtauf, M., Lingelbach, B., Wrist, E.R. (1997)
// The scintillating grid illusion. Vision Research,
// 37, 1033-1038.
// JS port (p5.js 'instance mode') by Jean Pierre Charalambos
import React from "react";
import Sketch from "react-p5";

const ScintillatingGrid =(props)=> {
  const setup = (p5, canvasParentRef) => {
    p5.createCanvas(400, 400).parent(canvasParentRef);
    p5.strokeWeight(3);        // medium weight lines 
    p5.smooth();               // antialias lines
    p5.noLoop();
	};
	const draw = (p5) => {
        p5.background(0);
        p5.stroke(100, 100, 100);  // dark grey colour for lines
        
        let step = 25;        
        //vertical lines
        for (let x = step; x < p5.width; x = x + step) {
            p5.line(x, 0, x, p5.height);
        }
    
        //horizontal lines
        for (let y = step; y < p5.height; y = y + step) {
            p5.line(0, y, p5.width, y);
        }
    
        // Circles
        p5.ellipseMode(p5.CENTER);
        p5.stroke(255, 255, 255);  // white circles
        for (let i = step; i < p5.width -5; i = i + step) {
            for (let j = step; j < p5.height -15; j = j + step) {
                p5.strokeWeight(6); 
                p5.point(i, j);
                p5.strokeWeight(3); 
            }
        }            
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default ScintillatingGrid;