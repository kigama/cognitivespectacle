import React from "react";
import Sketch from "react-p5";

const MotionBinding =(props)=> {
let d = 50.0*(1.4142135623730950488016887242097);
let delta = 15;
let lx = [400+delta, 500+delta, 300-d+delta, 400-d+delta, 300-d/2.0, 400-d/2.0, 400+d/2.0, 500+d/2.0];
let ly = [2.0*d-delta, 100+2.0*d-delta, 100+3.0*d-delta, 200+3.0*d-delta, 100+(3.0*d/2.0), (3.0*d/2.0), 200+(5.0*d/2.0), 100+(5.0*d/2.0)];
let limits = [ [400+delta, 400+d-delta ], [300-d+delta, 300-delta] ];
let speed = [ (d-delta)/45.0, -(d-delta)/45.0 ];
	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(800, 500).parent(canvasParentRef);
    p5.background(127);
	};
	const draw = (p5) => {
    p5.background(127);
    p5.stroke(0,0,255);
    p5.strokeCap(p5.SQUARE);
    p5.strokeWeight(6);
    makeLinesTypeOne(p5);
    makeLinesTypeTwo(p5);
    if( p5.mouseIsPressed ) {
      makeRectangles(p5);
    }       
  };
  function makeLinesTypeOne(p5) {
    for(let i = 0; i < 2; i++) { 
      p5.line( lx[2*i], ly[2*i], lx[2*i+1], ly[2*i+1] );
      lx[2*i] += speed[0];
      lx[2*i+1] += speed[0];
      ly[2*i] -= speed[0];
      ly[2*i+1] -= speed[0];
    }
    if( lx[0] < limits[0][0] || lx[0] > limits[0][1] ) speed[0] *= (-1.0); 
  }

  function makeLinesTypeTwo(p5) {
    for(let i = 2; i < 4; i++) {
      p5.line( lx[2*i], ly[2*i], lx[2*i+1], ly[2*i+1] );
      lx[2*i] += speed[1];
      lx[2*i+1] += speed[1];
      ly[2*i] += speed[1];
      ly[2*i+1] += speed[1];
    }
    if( lx[4] < limits[1][0] || lx[4] > limits[1][1] ) speed[1] *= (-1.0);
  }

  function makeRectangles(p5) {
    p5.noStroke();
    p5.fill(0,255,0);
    let cx = [400,500+d,400,300-d];
    let cy = [0,100+d,200+2.0*d,100+d];
    for(let i = 0; i < 4; i++) {
      p5.beginShape();
      p5.vertex( cx[i], cy[i] );
      p5.vertex( cx[i]+d, cy[i]+d );
      p5.vertex( cx[i], cy[i]+2.0*d );
      p5.vertex( cx[i]-d, cy[i]+d );
      p5.endShape();  
    }
  };
  return(<Sketch setup={setup} draw={draw}/>)

}
export default MotionBinding;
