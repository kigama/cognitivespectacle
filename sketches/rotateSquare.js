/**
  * Author: Carlos Tolet Bonilla
  * September 20, 2017
  *
  * Rotate Square Illusion
  *
**/
import React from "react";
import Sketch from "react-p5";

const RotateSquare =(props)=> {
    let angle = 0;
    let speed = 0.06;
  	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(600, 600).parent(canvasParentRef);
	};
	const draw = (p5) => {
		
        p5.background(255, 255, 255);
        rotateSquare(p5);
        //Función principal, pinta los cuadros color naranja
        if (!p5.mouseIsPressed) {
            p5.strokeWeight(0);
            p5.stroke(0);
            p5.fill(255, 140, 0);

            p5.rect(0, 0, 281, 281);
            p5.rect(318, 0, 281, 281);
            p5.rect(0, 318, 281, 281);
            p5.rect(318, 318, 281, 281);
        }
    };

    //Función que pinta el cuadro azul que rota en el fondo
    function rotateSquare(p5) {
        p5.push();
        angle += speed; //Se asigna el angulo de rotación con la velocidad

        p5.strokeWeight(0);
        p5.stroke(0);
        p5.fill(0, 0, 255);
        p5.translate(p5.width / 2, p5.height / 2);
        p5.rotate(angle);
        p5.rect(-187.5, -187.5, 375, 375);
        p5.pop();        
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default RotateSquare;
