import React from "react";
import Sketch from "react-p5";

const Anstis =(props)=> {
let rx = 100;
let rspeed = 1.00;  
	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(600, 400).parent(canvasParentRef);
	};
	const draw = (p5) => {
    p5.background(255); 
    if( !p5.mouseIsPressed )
    makeBars(p5);
    moveRect(p5);        

  };
  function moveRect(p5) {
    p5.noStroke();
    p5.fill(255,255,0);
    p5.rect( rx,120, 60,30 );
    p5.fill(0,0,102);
    p5.rect( rx,220, 60,30 );
    rx += rspeed;
    if( rx+60 >= 600 || rx <= 10 ) rspeed *= (-1.0);
  };

  function makeBars(p5) {
    p5.strokeCap(p5.SQUARE);
    p5.strokeWeight(15);
    p5.stroke(0);
    for(let i = 0; i < 20; i++) 
      p5.line(10+i*30,495-100,10+i*30,5);
  };
  return(<Sketch setup={setup} draw={draw}/>)
}

export default Anstis;