//Víctor Ramírez, Marzo 02, 2017
//Implementación desde cero
import React from "react";
import Sketch from "react-p5";

const Whites =(props)=> {
  const setup = (p5, canvasParentRef) => {
    p5.createCanvas(400, 400).parent(canvasParentRef);
	};
	const draw = (p5) => {
        p5.background( 0 );
        p5.noStroke();
        let rectW = p5.width/5.0;
        let rowHeight = 20.0;
        let rectColor = 100.0;
        let cur = 0; 
        let backColor = (p5.mouseIsPressed) ? 0 : 255;
        for( let y = 0; y < p5.height; y += rowHeight ){
            if( cur == 0 ){
                p5.fill( backColor );
                p5.rect( 0, y, rectW, rowHeight );
                p5.rect( 2*rectW, y, 3*rectW, rowHeight );
                p5.fill( rectColor );
                p5.rect( rectW, y, rectW, rowHeight );
            }
            else{
                p5.fill( rectColor );
                p5.rect( 3*rectW, y, rectW, rowHeight );
            }
            cur = (cur + 1) % 2;
        }
    };
  return(<Sketch setup={setup} draw={draw}/>)

}
export default Whites;