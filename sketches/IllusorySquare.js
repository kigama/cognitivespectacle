
import React from "react";
import Sketch from "react-p5";

const IllusorySquare =(props)=> {
    /*
    explanation: http://www.geek.com/news/10-astonishing-optical-illusion-gifs-1575117/ 
    Implemented by: Fabián Monsalve
    */

    var outside = 170;
    var inside = 25;
    var shift = true;

	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(600, 450).parent(canvasParentRef);
	};
	const draw = (p5) => {
		p5.background(0);

    p5.rectMode(p5.CENTER);
    p5.noFill();
    p5.stroke(255);
    p5.rect(p5.width / 2, p5.height / 2, 150, 150);

    p5.fill(150);
    p5.noStroke();
    // elipses grandes
    p5.ellipse((p5.width / 2), (p5.height / 2) - outside, 100, 100);
    p5.ellipse((p5.width / 2), (p5.height / 2) + outside, 100, 100);
    p5.ellipse((p5.width / 2) - outside, (p5.height / 2), 100, 100);
    p5.ellipse((p5.width / 2) + outside, (p5.height / 2), 100, 100);

    p5.fill(255);
    p5.noStroke();
    // elipses peequeñas
    p5.ellipse((p5.width / 2), (p5.height / 2) - inside, 30, 30);
    p5.ellipse((p5.width / 2), (p5.height / 2) + inside, 30, 30);
    p5.ellipse((p5.width / 2) - inside, (p5.height / 2), 30, 30);
    p5.ellipse((p5.width / 2) + inside, (p5.height / 2), 30, 30);


    if (shift) {
      if (outside >= 170) {
        shift = false;
        outside -= 0.9;
        inside += 0.9;
      } else {
        outside += 0.9;
        inside -= 0.9;
      }
    } else {
      if (outside <= 85) {
        shift = true;
        outside += 0.9;
        inside -= 0.9;
      } else {
        outside -= 0.9;
        inside += 0.9;
      }    
  }     
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default IllusorySquare;

