// Silencing
// Author: Jordan Suchow and George Alletez
// January 06 of 2011
// Motion Silences Awareness of Visual Change
// http://www.cell.com/current-biology/abstract/S0960-9822(10)01650-7
// https://en.wikipedia.org/wiki/Motion_silencing_illusion
// JS port (p5.js 'instance mode') by Sergio Andres Castro
import React from "react";
import Sketch from "react-p5";

const Silencing =(props)=> {
  let coord2 = [];
  let colorstart2 = [];
  let colorx = 0;  
  const setup = (p5, canvasParentRef) => {
    p5.createCanvas(400, 400).parent(canvasParentRef);
    circleOfCircles(p5,80, 50, 125, 20, coord2, colorstart2);
	};
	const draw = (p5) => {
    let rg1 = 0;
    let rs1 = p5.random(0.5, 1);
		p5.background(204);
    p5.push();
    p5.translate(p5.width/2.0, p5.height/2.0);
    p5.rotate(p5.radians(rg1+rs1));
    p5.push();
    p5.strokeWeight(5);
    p5.stroke(255);
    p5.point(0, 0);
    p5.pop();
    for (let i = 0; i < coord2.length; i +=1) {
      p5.push();
      p5.colorMode(p5.HSB);
      p5.noStroke();
      p5.fill((colorstart2[i]+(colorx))%255, 255, 255);
      let t = coord2[i];
      p5.ellipse(t[0], t[1], 10, 10);
      p5.pop();
    }
    p5.pop();
    colorx= colorx + 0.5;
    if ( !p5.mouseIsPressed ) {
      if (p5.millis() % 0.5 == 0) {
        rs1*=-1;
      }
      rg1+=(2*rs1);

    }        
	};
  function circleOfCircles(p5, n, minr, maxr, indr, coord, colorstart) {
    let xp = 0.0;
    let yp = 0.0;
    for (let i = 0; i < n; i += 1) {
      p5.push();
      p5.noStroke();
      xp = p5.random(-maxr, maxr);
      yp = p5.random(-maxr, maxr);
      if (p5.pow(xp, 2)+p5.pow(yp, 2) < p5.pow(maxr, 2) && p5.pow(xp, 2)+p5.pow(yp, 2) > p5.pow(minr, 2)) {
        let temp = 1;
        for (let j = 0; j < coord.length; j+=1) {
          if (p5.pow(xp-coord[j][0], 2)+p5.pow(yp-coord[j][1], 2) <= p5.pow(indr, 2)) {
            temp = 0;
            break;
          };
        };
        if (temp == 1) {
          colorstart.push(p5.random(255));
          let t = [xp, yp];
          coord.push(t);
        } else {
          i-=1;
        }
      } else {
        i-=1;
      }
      p5.pop();
    };
    
  };
  return(<Sketch setup={setup} draw={draw}/>)

}
export default Silencing;
