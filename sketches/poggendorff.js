//Víctor Ramírez, Marzo 02, 2017
//Implementación desde cero
import React from "react";
import Sketch from "react-p5";

const Poggendorff =(props)=> {
  
	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(400, 400).parent(canvasParentRef);
	};
	const draw = (p5) => {
		p5.background( 255 );
        let rectWidth = 50.0;//ancho del rectangulo
        p5.strokeWeight(3);
        //coordenadas del rectangulo
        let x1 = (p5.width - rectWidth) / 2.0;
        let x2 = (p5.width + rectWidth) / 2.0;
        let y1 = 20.0;
        let y2 = p5.height - y1;
        let d = 100.0;//distancia en x de los extremos de la linea al lado mas cercano del rectangulo
        let z = 25.0;//distancia entre rectas paralelas

        p5.stroke( 255, 0, 0 );
        p5.line( x1-d, y1, (x1+x2)/2.0, (y1+y2)/2.0 );
        p5.stroke( 0, 255, 0 );
        p5.line( x2+d, y2, (x1+x2)/2.0, (y1+y2)/2.0 );
        p5.stroke( 0, 0, 255 );
        p5.line( z*(x1 - 2*d - x2)/(y1-y2) + d + x2, y2, (x1+x2)/2.0, (y1+y2)/2.0-z );

        if( !p5.mouseIsPressed ){
            p5.noStroke();
            p5.fill( 100 );
            p5.rect( x1, y1, x2-x1, y2-y1 );
        }      
	};
  return(<Sketch setup={setup} draw={draw}/>)

}
export default Poggendorff;