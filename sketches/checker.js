import React from "react";
import Sketch from "react-p5";

const Checker =(props)=> {
  	const setup = (p5, canvasParentRef) => {
    p5.createCanvas(600, 600).parent(canvasParentRef);
	};
	const draw = (p5) => {
        p5.background(255);     
        checker(p5);
  };
      //Función principal, encargada de pintar la cuadricula de fondo
      function checker(p5) {

        let step = p5.height / 10;
        let doubleStep = step * 2;
        let random;
        let pattern = "01011010010"; //Patrón encargado de pintar los rombos entre la cuadricula

        p5.background(192, 255, 255);

        p5.strokeWeight(0);
        p5.stroke(0, 0, 0);
        p5.fill(0, 255, 0);

        for (let i = 0; i <= p5.width; i = i + doubleStep) {
            for (let j = 0; j <= p5.height; j = j + doubleStep) {
                p5.rect(i, j, step, step);
            }
        }

        for (let i = step; i <= p5.width; i = i + doubleStep) {
            for (let j = step; j <= p5.height; j = j + doubleStep) {
                p5.rect(i, j, step, step);
            }
        }

        if (!p5.mouseIsPressed) {
            for (let y = 0; y < pattern.length; y++) {
                for (let x = 0; x < pattern.length; x++) {
                    let ch = pattern.charAt(x);
                    quadChecker(p5, x * step, y * step, step, ch == '0');
                }
                pattern = rotateString(p5,pattern);
            }
        }
    }

    //Función que rota el patrón
    function rotateString(p5, s) {
        let lastChar = s.charAt(s.length - 1);
        return String(lastChar + s.substring(0, s.length - 1));
    }

    //Función que recibe como parámetros la posición en la cual se pintarán los rombos
    function quadChecker(p5,stepX, stepY, distance, position) {
        let height = distance / 5;
        let mheight = height / 2;

        p5.strokeWeight(0);
        p5.stroke(0, 0, 0);

        if (position) {
            p5.fill(0, 0, 0);
            p5.quad(stepX, stepY - height, stepX + mheight, stepY - mheight, stepX, stepY, stepX - mheight, stepY - mheight);
            p5.quad(stepX, stepY, stepX + mheight, stepY + mheight, stepX, stepY + height, stepX - mheight, stepY + mheight);

            p5.fill(255, 255, 255);
            p5.quad(stepX - mheight, stepY - mheight, stepX, stepY, stepX - mheight, stepY + mheight, stepX - height, stepY);
            p5.quad(stepX + mheight, stepY - mheight, stepX + height, stepY, stepX + mheight, stepY + mheight, stepX, stepY);
        } else {
            p5.fill(255, 255, 255);
            p5.quad(stepX, stepY - height, stepX + mheight, stepY - mheight, stepX, stepY, stepX - mheight, stepY - mheight);
            p5.quad(stepX, stepY, stepX + mheight, stepY + mheight, stepX, stepY + height, stepX - mheight, stepY + mheight);

            p5.fill(0, 0, 0);
            p5.quad(stepX + mheight, stepY - mheight, stepX + height, stepY, stepX + mheight, stepY + mheight, stepX, stepY);
            p5.quad(stepX - mheight, stepY - mheight, stepX, stepY, stepX - mheight, stepY + mheight, stepX - height, stepY);

        }
    }
  return(<Sketch setup={setup} draw={draw}/>)

}
export default Checker;